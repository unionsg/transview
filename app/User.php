<?php

namespace App;

use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $table = 'tv_users';

     public function setPasswordAttribute($password)
    {
         //$result = DB::executeFunction('return_hash(:binding_1)', [':binding_1' => $password], PDO::PARAM_LOB);
         $result = DB::selectOne("select return_hash('$password') as value from dual");
       // return $result;
        $this->attributes['password'] = $result->value;
    }

    public function oracleHash(string $value){
        $result = DB::executeFunction('return_hash(:binding_1)', [':binding_1' => $value], PDO::PARAM_LOB);
        return $result;
    }


}
