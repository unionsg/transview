<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Victorybiz\GeoIPLocation\GeoIPLocation;
use Auth;
use Location;
use Browser;
use DB;
use App\User;
use App\TvuserAudit;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    
     public function login(Request $request)
    {
       //return $request;
         $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->authenticate($request)) {
            $request->session()->regenerate();

            $this->clearLoginAttempts($request);
            //return redirect('home');
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $email = strtoupper($request->email);
        $password = $request->password;

        $user = User::Where('email',$email)->first();       
        

       if($user){
          $geoip = new GeoIPLocation(); 
          $audit = new TvuserAudit();
          $audit->user_id = $user->id;
          $audit->operation_type = 'LOGIN';
          $audit->status = '0';
          $audit->ip = $request->getClientIp(true);
          $audit->country = $geoip->getCountry();
          $audit->os = Browser::platformName();
          $audit->browser = Browser::browserName(); 
          $audit->device = Browser::deviceFamily();
            if($this->hashCheck($password, $user->password)) {
                Auth::login($user);
                $audit->status = '1';
                $audit->save();
                return true;
            }
          $audit->save();
          }

            return false;
        
    }


    protected function hashCheck($value,$hashedValue){
        if (strlen($hashedValue) === 0) {
            return false;
        }

        $value = $this->oracleHash($value);

        if($value === $hashedValue){
            return true;
        }else{
            return false;
        }
    }

     protected function oracleHash($password)
    {        
        $result = DB::selectOne("select return_hash('$password') as value from dual");
        return $result->value;       
    }

      /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
         $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

     /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

     public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }

  
     /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return view('auth.logout');

    }

    
}
