<?php

namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\User;
use App\UsedFunction;

class SetupPasswordController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Setup inital password and set security question
     * This is for first time logins
     *
     */
    public function loginSetup(Request $request)
    {
        // return $this->oracleHash($request->password);
    	$this->validateSetup($request);

       if ($this->authenticateSetup($request)) {
		 	//go to home page
	       return redirect()->route('home');
	    }

	    $errors = [
	    			'password' => 'Password mismatch'
	    		];

        return redirect('home')->with('errors','Password mismatch');
    }

     /*
     * Validate etac before login
     * This is for every login
     *
     */
    public function etacLogin(Request $request)
    {
    	$code = $request->etac;
    	$func = new UsedFunction();

    	if($func->validateEtac(Auth::id(),$code))
    	{
    		return 'andy';
    	}

    	return view('auth.passwords.etac');
    }


    /*
    *  
    *
    */    
    protected function oracleHash($password)
    {        
        $result = DB::selectOne("select return_hash('$password') as value from dual");
        return $result->value;       
    }

     /**
     * Validate the initail security setup request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateSetup(Request $request)
    {
        $request->validate([
            'password1' => 'required|string',
            'password' => 'required|string'
        ]);
    }

    public function authenticateSetup(Request $request)
    {
    	$password = $request->password;
    	$password1 = $request->password1;      

    	if($password == $password1)
    	{
    		$user = User::find(Auth::id());

    		if($user){
    			
    			$user->password = $password;
    			$user->f_login = 'N';
    			$user->save();

    			return true;
    		}
    		return false;
    	}
    	
    	
    	return false;
    	
    }

}
