<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

     public function randomHex() {
        $chars = 'ABCDEF0123456789';
        $color = '#';
        for ( $i = 0; $i < 6; $i++ ) {
          $color .= $chars[rand(0, strlen($chars) - 1)];
        }
        return $color;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if(Auth::user()->f_login=='Y'){
            return view('auth.passwords.change');
        }

     $acct_info = DB::table('vw_tv_useraccounts')
                         ->where([                               
                                ['USER_ID', '=', Auth::id()]
                                ])                       
                        ->get();

   foreach ($acct_info as $acct_inf){
            $bal[] = $acct_inf->av_bal_lc;
            $labl[] = $acct_inf->acct_id;
            $mcolor[] = $this->randomHex();
        }

    $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 400, 'height' => 200])
        ->labels($labl)
        ->datasets([
            [
                'backgroundColor' => $mcolor,
                'hoverBackgroundColor' => $mcolor,
                'data' => $bal
            ]
        ])
        ->options([]);

        return view('main',['acct_infos' => $acct_info,
                            'chartjs'=>$chartjs,
                            ]);
    }
}
