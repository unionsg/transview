<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes(['verify'=>true]);
Route::get('logout', 'Auth\LoginController@logout');
Route::post('secsetup','Auth\SetupPasswordController@loginSetup')->name('setup');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');


Route::post('acct-det','AccountController@acct_trans')->name('account-trans');
Route::post('acct-ddet','AccountController@acct_dtrans')->name('account-dtrans');