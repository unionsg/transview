@extends('layouts.master')

@section('styles')

@endsection

@section('content')
<!-- breadcrumbs -->
<div class="bg-white">
    <div class="container">
        <ol class="breadcrumb breadcrumb-alt">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
    </div>
</div>

<div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">

    <div class="row">
        <div class="col-lg-12 col-sm-12 d-flex flex-column">
            <div class="card social-card share  full-width m-b-10 no-border" data-social="item">
                <div class="card-header ">
                    <h5 class="text-complete pull-left fs-12">
                       <i class="fa fa-circle text-complete fs-11"></i> Accounts Graphical Summary
                    </h5>
                   <!--  <div class="pull-right small hint-text">
                    5,345 <i class="fa fa-comment-o"></i>
                    </div> -->
                    <div class="clearfix"></div>
                </div>
                <div class="card-description">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            <div id="sparkline-pie" class="sparkline-chart m-t-40">
                                hjh
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <!-- Graph Keys -->
                          <div class="task clearfix row">
                                <div class="task-list-title col-10 justify-content-between">
                                  <a href="#" class="text-master" data-task="name">Purchase Pages before 10am
                                  </a>
                                  <i class="fs-14 pg-close hidden"></i>
                                </div>
                          </div>
                          <div class="task clearfix row">
                                <div class="task-list-title col-10 justify-content-between">
                                  <a href="#" class="text-master" data-task="name"><span class="badge badge-success">.</span> Purchase Pages before 10am
                                  </a>                                 
                                </div>
                          </div>
                          <div class="task clearfix row">
                                <div class="task-list-title col-10 justify-content-between">
                                  <a href="#" class="text-master" data-task="name"><span class="badge badge-success">.</span> Purchase Pages before 10am
                                  </a>                                 
                                </div>
                          </div>
                          <div class="task clearfix row">
                                <div class="task-list-title col-10 justify-content-between">
                                  <a href="#" class="text-master" data-task="name"><span class="badge badge-success">.</span> Purchase Pages before 10am
                                  </a>                                 
                                </div>
                          </div>
                        </div>
                    </div>
                    
                </div>               
            </div>
        </div>
    </div>

    <!-- Tab two -->
    <div class="row">
        <div class="col-lg-12 col-sm-12 d-flex flex-column">
            <div class="card social-card share  full-width m-b-10 no-border" data-social="item">
                <div class="card-header ">
                    <h5 class="text-complete pull-left fs-12">
                       <i class="fa fa-circle text-complete fs-11"></i> Accounts Balance Summary
                    </h5>
                   <!--  <div class="pull-right small hint-text">
                    5,345 <i class="fa fa-comment-o"></i>
                    </div> -->
                    <div class="clearfix"></div>
                </div>
                <div class="card-description">
                    <div class="row">
                        
                        <table class="table table-hover demo-table-dynamic table-responsive-block">
                            <thead>
                                <tr>
                                    <th>BBAN</th>
                                    <th>ACCOUNT NAME</th>
                                    <th>LEDGER BALANCE</th>
                                    <th>AVAILABLE BALANCE</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>003001119499110142</td>
                                    <td>Union Systems Global</td>
                                    <td>SLL 15,000,000</td>
                                    <td>SLL 15,000,000</td>
                                    <td><button class="btn btn-info">DETAILS</button></td>
                                </tr>
                                 <tr>
                                    <td>003001119499110142</td>
                                    <td>Union Systems Global</td>
                                    <td>SLL 15,000,000</td>
                                    <td>SLL 15,000,000</td>
                                    <td><button class="btn btn-info">DETAILS</button></td>
                                </tr>
                                 <tr>
                                    <td>003001119499110142</td>
                                    <td>Union Systems Global</td>
                                    <td>SLL 15,000,000</td>
                                    <td>SLL 15,000,000</td>
                                    <td><button class="btn btn-info">DETAILS</button></td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>
                    
                </div>               
            </div>
        </div>
    </div>

</div>

@endsection

@section('scripts')
<script src="{{asset('js/charts.js')}}" type="text/javascript"></script>
@endsection