<!DOCTYPE html>
<html lang="en">
    
	<head>
        <meta charset="utf-8" />
        <title>SLCB-TransView</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->

        @include('snippets.style')
        <style type="text/css">
	        .content-page {
			     margin-left: 0px !important; 
			 }

			 .navbar-custom {
		    	background-color: #e82b34!important;
		    	color: white;
			}
        </style>
		@yield('styles')
      
    </head>
    <body>

    	<!-- Begin page -->
        <div id="wrapper">
        	@include('snippets.nav')
        	 <div class="content-page">
                <div class="content">
                	@yield('content')
                </div>
            </div>
        </div>
        @include('snippets.script')
		@yield('scripts')
    </body>
</html>
