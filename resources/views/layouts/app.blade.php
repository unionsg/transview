<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from coderthemes.com/ubold/layouts/light/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Mar 2019 09:53:38 GMT -->
<head>
        <meta charset="utf-8" />
        <title>{{ env('app_name', 'Ok') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App css -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body class="authentication-bg authentication-bg-pattern">

        <div class="account-pages mt-5 mb-5">
            <div class="container card">
                <div class="row">
                    <div class="col-md-8 col-lg-6 col-xl-7" style="padding-left: 0px">
                        <img class="img-fluid" src="{{asset('assets/images/meeting.jpg')}}">
                    </div>
                    <div class="col-md-4 col-lg-6 col-xl-5">
                        <div class="">
                              @yield('content')
                        </div>
                        <!-- end card -->
                        
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->


        <footer class="footer footer-alt" style="color: black">
            &copy; {{date('Y')}}  <a href="http://www.slcb.com/" >SLCB</a>  ...delivering value
        </footer>

        <!-- Vendor js -->
        <script src="{{asset('assets/js/vendor.min.js')}}"></script>

        <!-- App js -->
        <script src="{{asset('assets/js/app.min.js')}}"></script>
        
    </body>

<!-- Mirrored from coderthemes.com/ubold/layouts/light/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Mar 2019 09:53:38 GMT -->
</html>