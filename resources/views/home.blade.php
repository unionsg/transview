@extends('layouts.master')

@section('styles')

@endsection

@section('content')

<!-- Start Content-->
<div class="container">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="form-inline">
                        <div class="form-group">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control border-white" id="dash-daterange">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                            <i class="mdi mdi-autorenew"></i>
                        </a>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-1">
                            <i class="mdi mdi-filter-variant"></i>
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                             <table style="margin-left: 30px;line-height: 35px;margin-top: 20px">
                                 <tr>
                                     <td style="text-align: right;font-weight: bold;">Account Name</td>
                                     <td>:</td>
                                     <td>Union Systems Global</td>
                                 </tr>
                                  <tr>
                                     <td style="text-align: right;font-weight: bold;">Currency</td>
                                     <td>:</td>
                                     <td>SLL</td>
                                 </tr>
                                  <tr>
                                     <td style="text-align: right;font-weight: bold;">Ledger Balance</td>
                                     <td>:</td>
                                     <td>12,000,000</td>
                                 </tr>
                                 <tr>
                                     <td style="text-align: right;font-weight: bold;">Available Balance</td>
                                     <td>:</td>
                                     <td>12,000,000</td>
                                 </tr>
                             </table>
                        </div>
                        <div class="col-md-6" style="margin-top: 30px">
                                <div id="morris-donut-example" style="height: 120px;" class="morris-chart mt-3"></div> 
                        </div>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div> <!-- end row -->

     <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                    hakk
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div> <!-- end row -->

    <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th>Posting Date</th>
                                <th>Value Date</th>
                                <th>Transaction Description</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>06-MAR-2018</th>
                                <td>06-MAR-2018</td>
                                <td>Payment of toyota truck</td>
                                <td>15,000,000</td>
                                <td></td>
                                <td>100,500,100</td>
                            </tr>
                            <tr>
                                <th>06-MAR-2018</th>
                                <td>06-MAR-2018</td>
                                <td>Payment of toyota truck</td>
                                <td></td>
                                <td>15,000,000</td>
                                <td>100,500,100</td>
                            </tr>
                            <tr>
                                <th>06-MAR-2018</th>
                                <td>06-MAR-2018</td>
                                <td>Payment of toyota truck</td>
                                <td>15,000,000</td>
                                <td></td>
                                <td>100,500,100</td>
                            </tr>
                          
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div> <!-- end row -->

</div>


@endsection

@section('scripts')
 
 <script src="{{asset('assets/libs/morris-js/morris.min.js')}}"></script>
 <script src="{{asset('assets/libs/raphael/raphael.min.js')}}"></script>
 <script src="{{asset('assets/js/pages/dashboard-1.init.js')}}"></script>

@endsection