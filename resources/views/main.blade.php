@extends('layouts.master')

@section('styles')

@endsection

@section('content')

<!-- Start Content-->
<div class="container">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="form-inline">
                        <div class="form-group">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control border-white" id="dash-daterange">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                            <i class="mdi mdi-autorenew"></i>
                        </a>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-1">
                            <i class="mdi mdi-filter-variant"></i>
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                        <h4 class="header-title mb-0">Graphical Representation of Available Balances</h4>
                    <div class="row">
                        <div class="col-md-6">
                               {!! $chartjs->render() !!}
                        </div>
                        <div class="col-md-6" style="margin-top: 30px">
                            
                        </div>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div> <!-- end row -->

     <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th>Account Name</th>
                                <th>BBAN</th>
                                <th>Currency</th>
                                <th>Ledger Balance</th>
                                <th>Available Balance</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($acct_infos as $acct_info)
                                <tr>
                                    <th>{{$acct_info->acct_name}}</th>
                                    <td>{{$acct_info->acct_id}}</td>
                                    <td>{{$acct_info->currency}}</td>
                                    <td style="text-align: right;">{{number_format($acct_info->ledger_bal,2, '.', ',')}}</td>
                                    <td style="text-align: right;">{{number_format($acct_info->av_bal,2, '.', ',')}}</td>
                                    <td>
                                        <form method="POST" action="{{url('acct-det')}}">
                                            <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
                                            <input type="hidden" id="act" name="acct" value="{{$acct_info->acct_id}}">
                                            <button type="submit" class="btn btn-sm btn-primary">Details</button>
                                        </form>                                    
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div> <!-- end row -->

</div>


@endsection

@section('scripts')
 
 <script src="{{asset('assets/libs/morris-js/morris.min.js')}}"></script>
 <script src="{{asset('assets/libs/raphael/raphael.min.js')}}"></script>
 <!-- <script src="{{asset('assets/js/pages/dashboard-1.init.js')}}"></script> -->
 <script type="text/javascript">
     
 </script>

@endsection