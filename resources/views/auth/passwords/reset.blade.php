@extends('layouts.app')

@section('content')
<div class="card-body p-4">
                                
    <div class="text-center w-75 m-auto">
        <a href="index.html">
            <span><img src="{{asset('assets/images/slcb_sm.png')}}" alt="" ></span>
        </a>
        <p class="text-muted mb-4 mt-3">
            {{ __('Reset Password') }}
        </p>
    </div>

   <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group mb-3">
                            <label for="emailaddress">Email address</label>
                              <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Enter your email">
                              @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                         <div class="form-group mb-3">
                            <label for="emailaddress">{{ __('Password') }}</label>
                              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Enter your new password">
                              @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group mb-3">
                            <label for="emailaddress">{{ __('Confirm Password') }}</label>
                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm your new password">
                             
                        </div>

                        
                       <div class="form-group mb-0 text-center">
                           <button class="btn btn-primary btn-block" type="submit">{{ __('Reset Password') }} </button>
                       </div>
                       
                    </form>

   

</div> <!-- end card-body -->

@endsection
