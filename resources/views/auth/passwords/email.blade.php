@extends('layouts.app')

@section('content')

<div class="card-body p-4">
                                
    <div class="text-center w-75 m-auto">
        <a href="index.html">
            <span><img src="{{asset('assets/images/slcb_sm.png')}}" alt="" ></span>
        </a>
        <p class="text-muted mb-4 mt-3">
       {{ __('Reset Password') }}
        </p>
    </div>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <form method="POST" action="{{ route('password.email') }}">
        @csrf

        <div class="form-group mb-3">
            <label for="emailaddress">Email address</label>
              <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Enter your email">
              @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
        </div>
       
        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary btn-block" type="submit"> {{ __('Send Password Reset Link') }} </button>
        </div>

        <div class="row mt-3">
            <div class="col-12 text-center">
                <p><a href="{{url('/login')}}" >Click here to <b style="font-weight: bold;color: red">homepage</b></a></p>
            </div> <!-- end col -->
        </div>

        
    </form>

   

</div> <!-- end card-body -->

@endsection
