@extends('layouts.app')

@section('content')

<div class="card-body p-4" style="line-height: 60px">

     <p >        
         <h4 class="header" style="color: red"> <i class="fa fa-lock"> </i> Security Setup</h4>
         Please set your new password.
     </p>


     @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <form method="POST" action="{{url('secsetup')}}">
        @csrf

         <div class="form-group mb-4">
            <!-- <label for="emailaddress">New Password</label> -->
              <input id="password" type="password" class="form-control" name="password"  required placeholder="New Password">
        </div>

         <div class="form-group mb-4">
            <!-- <label for="emailaddress">Confirm Password</label> -->
              <input id="password" type="password" class="form-control" name="password1"required placeholder="Confirm Password">
            
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors}}</strong>
                    </span>
             
        </div>
         
        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary btn-block" type="submit"> Confirm </button>
        </div>

    </form>
   
</div> <!-- end card-body -->


@endsection
