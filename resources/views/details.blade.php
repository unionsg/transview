@extends('layouts.master')

@section('styles')
<link href="{{asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>

@endsection

@section('content')

<!-- Start Content-->
<div class="container">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="form-inline">
                        <div class="form-group">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control border-white" id="dash-daterange">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                            <i class="mdi mdi-autorenew"></i>
                        </a>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-1">
                            <i class="mdi mdi-filter-variant"></i>
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                         <div class="col-md-6">
                                {!! $chartjs->render() !!}
                        </div>
                        <div class="col-md-6">
                             <table style="margin-left: 30px;line-height: 35px;margin-top: 20px">
                                 <tr>
                                     <td style="font-weight: bold;">Account Name</td>
                                     <td style="padding-left: 20px;padding-right: 20px">:</td>
                                     <td>{{$acct_info->acct_name}}</td>
                                 </tr>
                                  <tr>
                                     <td style="font-weight: bold;">Currency</td>
                                     <td style="padding-left: 20px;padding-right: 20px">:</td>
                                     <td >{{$acct_info->currency}}</td>
                                 </tr>
                                  <tr>
                                     <td style="font-weight: bold;">Ledger Balance</td>
                                     <td style="padding-left: 20px;padding-right: 20px">:</td>
                                     <td>{{number_format($acct_info->ledger_bal,2, '.', ',')}}</td>
                                 </tr>
                                 <tr>
                                     <td style="font-weight: bold;">Blocked Amount</td>
                                     <td style="padding-left: 20px;padding-right: 20px">:</td>
                                     <td>{{number_format($acct_info->blk_amt,2, '.', ',')}}</td>
                                 </tr>
                                 <tr>
                                     <td style="font-weight: bold;">Available Balance</td>
                                     <td style="padding-left: 20px;padding-right: 20px">:</td>
                                     <td>{{number_format($acct_info->av_bal,2, '.', ',')}}</td>
                                 </tr>
                             </table>
                        </div>
                       
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div> <!-- end row -->

     <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <p>NB: The system load your last 10 transaction on fresh load.<br> You can use the date search to search for particular date range</p>
                        </div>
                        <div class="col-md-7">
                            <div id="datesearch">
                                <form method="POST" action="{{url('acct-ddet')}}">
                                 @csrf
                                 <input type="hidden" name="acct" value="{{$acct_info->acct_id}}">
                                     <div class="form-row align-items-center">
                                         <div class="col-auto">
                                              <label class="sr-only" for="inlineFormInputGroup">From</label>
                                             <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                     <span class="input-group-text far fa-calendar-alt"></span>
                                                 </div>
                                                 <input type="text" id="datefrom" name="from" required="" class="form-control" value="{{$dfrom == null ? ' ' : $dfrom}}">
                                             </div>                                    
                                             <!-- <input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Jane Doe"> -->
                                         </div>
                                          <div class="col-auto">
                                              <label class="sr-only" for="inlineFormInputGroup"></label>
                                             <div class="input-group mb-2">
                                                 <span class="">to</span>
                                             </div>                                    
                                         </div>
                                         <div class="col-auto">
                                             <label class="sr-only" for="inlineFormInputGroup">To</label>
                                             <div class="input-group mb-2">
                                                 <div class="input-group-prepend">
                                                     <div class="input-group-text far fa-calendar-alt"></div>
                                                 </div>
                                                 <input type="text" class="form-control" name="to" required="" id="dateto" value="{{$dto == null ? ' ' : $dto}}">
                                             </div>
                                         </div>
                                         <div class="col-auto">
                                             <button type="submit" class="btn btn-primary waves-effect waves-light mb-2">Search</button>
                                         </div>
                                     </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div> <!-- end row -->

    <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="transTable" class="table mb-0 dt-responsive">
                            <thead class="thead-light">
                            <tr>
                                <th>Posting Date</th>
                                <th>Value Date</th>
                                <th>Transaction Description</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($trans as $tran)
                            <tr>
                                <th>{{date('d-M-Y', strtotime($tran->post_date))}}</th>
                                <td>{{date('d-M-Y', strtotime($tran->value_date))}}</td>
                                <td>{{$tran->narration1}}</td>
                                <td style="text-align: right;">{{$tran->amount_debit==null ?  ' ' : number_format($tran->amount_debit,2, '.', ',')}}</td>
                                <td style="text-align: right;">{{$tran->amount_credit==null ?  ' ' : number_format($tran->amount_credit,2, '.', ',')}}</td>
                                <td style="text-align: right;">{{number_format($tran->balance,2, '.', ',')}}</td>
                            </tr>
                                @endforeach
                          
                          
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div> <!-- end row -->

</div>


@endsection

@section('scripts')
 
 <script src="{{asset('assets/libs/morris-js/morris.min.js')}}"></script>
 <script src="{{asset('assets/libs/raphael/raphael.min.js')}}"></script>
 <!-- <script src="{{asset('assets/js/pages/dashboard-1.init.js')}}"></script> -->
 <script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
 <!-- <script src="{{asset('assets/js/pages/form-pickers.init.js')}}"></script> -->

 <script src="{{asset('assets/libs/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<!-- <script src="{{asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script> -->
<!-- <script src="{{asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script> -->
<script src="{{asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<!-- <script src="{{asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script> -->
<script src="{{asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<!-- <script src="{{asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script> -->
<script src="{{asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>
<script type="text/javascript">
     ! function(e) {
    "use strict";
    var i = function() {};
    i.prototype.init = function() {
        e("#datefrom").flatpickr({
            altInput: !0,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d"
        }), e("#dateto").flatpickr({
            altInput: !0,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d"
        })
    }, e.FormPickers = new i, e.FormPickers.Constructor = i
}(window.jQuery),
function(e) {
    "use strict";
    e.FormPickers.init()
}(window.jQuery);


    $(document).ready( function () {
        $('#transTable').DataTable({
            dom: 'Bfrtip',
            lengthChange: !1,
            buttons: ["print", "csv","pdf"],
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });
    } );
 </script>

@endsection